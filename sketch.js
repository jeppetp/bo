var width = window.innerWidth
var height = window.innerHeight
// euclidean distance
let dist = ((p1, p2) => sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2))
// ...I miss python
let range = ((n) => [...Array(n).keys()])

function windowResized() {
  width = windowWidth
  height = windowHeight
  resizeCanvas(width, height)
}

function setup() {
  createCanvas(windowWidth, windowHeight)
  colorMode(HSB, 255)
  frameRate(30)
}

var t = 0
let colNum = 40
let sin_res = 128
let sin_height = height/2
map(x,0,sin_res,0,1)
map(y,0,colNum,0,1)

function amplitude(x, y) {
  return map(cos(-5*t+(map(mouseX,0,width,0, 100))*dist([map(x,0,sin_res,0,1),map(y,0,colNum,0,1)], [0.5,0.5])),-1,1,0,1)
}
function rings(x, y, t) {
  let rings = 13
  let rd = (height*0.7) / rings
  let speed = 100
  let magic = ((cos(t)*speed) - 60) * 2
  stroke(100, 50, 190)
  noFill()
  for (var i=1; i<rings; i++) {
    stroke(map(rd*i + magic, -width/2, width/2, 0, 255),50,150)
    // circle(x, y, (rd*i + magic)*3)
    //circle(x, y, rd*i)
  }
}

function draw() {
  let normY = map(mouseY, 0, height, -1, 1)
  blendMode(BLEND)
  background(100, 50, 60)
  blendMode(BLEND);
  fill(100, 50, 90)
  noStroke()
  let sin_offset = t
  for (i=0;i<colNum; i++){
    let y_offset = (i / colNum) * height/2 - height/2
    beginShape()
    vertex(width - 1,  height-1)
    vertex(0, height-1)
    vertex(0, height/2)
    for (j=0; j<sin_res; j++){
      fill(map(i, 0, colNum - 1, 0, 256)%256,100,150)
      // rect(0, 0, 100, 100)
      let x = map(j, 0, sin_res-1, 0, width-1)
      // let y = height/2 * amplitude(j,i+0.5)
      let y = map(amplitude(j,i+0.5), 0, 1, -sin_height*normY, sin_height*normY)
      // vertex(x, y - 175*noise((t*2) + x*0.1) + 100)
      vertex(x, y + map(i, 0, colNum, -sin_height, height + sin_height))

      //vertex(x, y)
    }
    endShape(CLOSE)
  }

  for (var ringx=0; ringx < width + 100; ringx += 50) {
    rings(ringx, height, t + (ringx * 0.002))
  }

  t+=0.03
}
